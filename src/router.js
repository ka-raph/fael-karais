import Vue from "vue";
import Router from "vue-router";

const Home = () => import("./views/Home.vue");
const About = () => import("./views/About.vue");
const Osaf = () => import("./views/Osaf.vue");
const BuyOsaf = () => import("./views/Osaf/Buy.vue");
const Extra = () => import("./views/Extra.vue");
const Summary = () => import("./views/Osaf/Summary.vue");
const wMap = () => import("./views/Osaf/Map.vue");
const Vocab = () => import("./views/Osaf/Vocab.vue");
const Extracts = () => import("./views/Osaf/Extracts.vue");
const InOsaf = () => import("./views/Extra/InOsaf.vue");
const ExtraOthers = () => import("./views/Extra/ExtraOthers.vue");
const Sales = () => import("./views/Sale.vue");

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  linkActiveClass: "is-active",
  linkExactActiveClass: "is-active-exact",
  routes: [
    {
      path: "/",
      component: Home
    },
    {
      path: "/about",
      component: About
    },
    {
      path: "/of-shadow-and-fire",
      component: Osaf
    },
    {
      path: "/of-shadow-and-fire/summary",
      component: Summary
    },
    {
      path: "/of-shadow-and-fire/buy",
      component: BuyOsaf
    },
    {
      path: "/of-shadow-and-fire/map",
      component: wMap
    },
    {
      path: "/of-shadow-and-fire/vocabulary",
      component: Vocab
    },
    {
      path: "/of-shadow-and-fire/extracts",
      component: Extracts
    },
    {
      path: "/extra-stories",
      component: Extra
    },
    {
      path: "/extra-stories/in-of-shadow-and-fire",
      component: InOsaf
    },
    {
      path: "/extra-stories/others",
      component: ExtraOthers
    },
    {
      path: "/sales",
      component: Sales
    },
    {
      path: "*",
      component: Home
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      };
    }
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        x: 0,
        y: 0
      };
    }
  }
});
