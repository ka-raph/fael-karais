# Author-blog
Welcome to my author blog ! I first thought of making a template out of it, but I'll use it for my own writings. It's only in Frenglish for now, but I'm working on translating it in full english & full french. For now it's a sample, I plan to add more and more content during the summer.

This project is a single page application that is built Vue.js and uses Bulma for the style.

## Project setup
```
npm install
```

**Compiles and hot-reloads for development**
```
npm run serve
```

**Compiles and minifies for production**
```
npm run build
```

**Lints and fixes files**
```
npm run lint
```

## Use the modals
Modals in this project are a Bulma component, but I customized it to fit my needs for this blog. There are two kinds of modals : image modals and text modals (a simple and not eye-aggressive-white-background reading view for book samples for instance, with a width not too wide to avoid getting trouble to go to the beginning of each new line).

A modal is triggered by an element with the following class : *btnModal*, it can be a classic button, span, block, link for the **text modals**, but for the **pictures** it's aimed to display a bigger version of an already existing and displayed picture.
The content is automatically added to the modal depending the type of modal you need. **You should always put the modal in the same parent node as its trigger**.
To use the modals, write the following tag right after your trigger :
```
<modals />
```
Add to the modals tag the class corresponding to the kind of modal you want :
- **Text** : *modalTxt*
- **Img** : *modalImg*

Ex :
```
<div>
    <span id="bookcoverimg" class="image btnModal">
        <img src="../assets/bookex.png" />
    </span>
    <modals class="modalImg" />
</div>
```
Note that the picture is in a span tag, the way to build the modals in my website is not something you can easily export because those were built regarding to this blog template.
#### Modals types & rules
**Text modals :**
> Each text modal should be separated from each other by at least 3 parent nodes and the text content should be maximum a child of the 3rd ancestor. The text block must have the class *textArea* in order to be rendered inside of the modal. Ex :
```
<div>
    <div>
        <div>
            <button class="btnModal">
                Reading mode
            </button>
            <modals class="modalTxt" />
        </div>
    </div>
    <div class="textArea">
        <p>Lorem ipsum</p>
    </div>
</div>
```

**Img modals :**
>The pictures you want to display should always be inside of a span, div or inside any other of these kind of tags. It's the content of those tags that will be displayed, allowing you to apply the style you want to the img container whithout altering the modal rendering. Ex:
```
<div>
    <div class="btnModal">
        <img src="../assets/mypicture.png" />
    </div>
    <modals class="modalImg" />
</div>
```